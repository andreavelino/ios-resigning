#!/usr/bin/env bash

# App to ReSign
app_name=$1

NON_IOS_APPS="Android\|PluginTestApp\|LUNAR-Player-1\|Speqs-iOS-App-Store\|FanCam"

verify_login_status () {
  # This function checks to see if the user is logged in or not

  if [[ $(appcenter profile list >/dev/null 2>&1; echo $?) == 5 ]]; then
    clear
    echo "You are not logged in to AppCenter."
    echo "Please login to AppCenter with your credentials."
    appcenter login
  fi
}

cleanup () {
  echo "Removing temporary files..."

  if [[ -d ${PWD}/_floatsignTemp ]]; then
    echo "Removing temporary files..."
    if [[ $(rm -rf ${PWD}/_floatsignTemp; echo $?) == 0 ]]; then
      echo "Temporary directory removed successfully."
    else
      echo "There were problems removing temporary directory."
    fi
  fi

  if [[ -d ${temp_dir} ]]; then
    echo "Removing temp directory..."
    rm -rf ${temp_dir}
    if [[ $(echo $?) == 0 ]]; then
      echo "Temp directory removed successfully."
    else
      echo "There was a problem removing the temp directory."
    fi
  fi
}

check_number_of_arguments () {
  if [ $# -eq 0 ]; then
    clear
    echo "Please provide the app name as first argument"
    echo "Usage:"
    echo "  im-os-resign.bash <app_name>"
    echo
    echo "List of apps to choose from:"
    appcenter apps list | grep -v "${NON_IOS_APPS}" | awk -F/ '{print $2}'
    exit 1
  fi
}

get_dependencies () {
  # This function attempts to verify and install any dependencies this script needs to run
  
  clear
  echo "Verifying script dependencies"

  # Verify Homebrew installation
  echo "Verifying homebrew installation..."
  if [[ $(which brew >/dev/null 2>&1; echo $?) != 0 ]]; then
    echo "homebrew is not installed.  Installing..."
    # Requires user to input password
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    if [[ $(echo $?) == 0 ]]; then
      echo "Homebrew was successfully installed."
    else
      echo "There was an error installing homebrew."
    fi
  fi

  # Verify npm installation
  echo "Verifying node/npm installation..."
  if [[ $(which node >/dev/null 2>&1; echo $?) != 0 ]]; then
    echo "Node is not installed.  Installing..."
    brew install node
    if [[ $(echo $?) == 0 ]]; then
      echo "Node was successfully installed."
    else
      echo "There was an error installing node."
    fi
  fi

  # Verify appcenter-cli installation
  echo "Verifying appcenter-cli installation..."
  if [[ $(which appcenter >/dev/null 2>&1; echo $?) != 0 ]]; then
    echo "Appcenter-cli is not installed.  Installing..."
    npm -g install appcenter-cli
    if [[ $(echo $?) == 0 ]]; then
      echo "Appcenter-cli was successfully installed."
    else
      echo "There was an error installing appcenter-cli."
      exit 1
    fi
  fi

  # Verifying fastlane installation
  echo "Verifying fastlane installation..."
  if [[ $(which fastlane >/dev/null 2>&1; echo $?) != 0 ]]; then
    echo "Fastlane is not installed.  Installing..."
    brew cask install fastlane
    if [[ $(echo $?) == 0 ]]; then
      echo 'export PATH="$HOME/.fastlane/bin:$PATH"' >> $HOME/.bash_profile
      source $HOME/.bash_profile
      echo "Fastlane was successfully installed."
    else
      echo "There was an error installing fastlane."
      exit 1
    fi
  fi

  # Verifying wget installation
  echo "Verifying installation of wget..."
  if [[ $(which wget >/dev/null 2>&1; echo $?) != 0 ]]; then
    echo "wget is not installed.  Installing..."
    brew install wget
    if [[ $(echo $?) == 0 ]]; then
      echo "wget was successfully installed."
    else
      echo "There was an error installing wget."
      exit 1
    fi
  fi

  # Verifying ssh access to BitBucket
  echo "Verifying ssh connection to BitBucket"
  if [[ $(ssh -T git@bitbucket.org >/dev/null 2>&1; echo $?) == 255 ]]; then
    echo "\"Permission Denied\" error when attempting ssh connection to BitBucket repository."
    if [[ $(echo $?) == 255 ]]; then
      clear
      echo "Permission Denied error when attempting ssh connection to BitBucket repository."
      echo "Please make sure you have generated ssh keys and your public key has been uploaded to BitBucket."
      exit 1
    fi
  fi
}

download_build () {
  # This function downloads the latest build of the app passed to the function.

  clear
  echo "Searching for ${app_name} in AppCenter..."

  # Set app to work with, if it's not already set.
  if ! [[ $(appcenter apps get-current | grep -q ${app_name}; echo $?) == 0 ]]; then
    if [[ $(appcenter apps list | grep -v "${NON_IOS_APPS}" | grep ${app_name} | head -n 1 >/dev/null 2>&1; echo $?) == 0 ]]; then
      full_app_name=$(appcenter apps list | grep -v "${NON_IOS_APPS}" | grep ${app_name} | head -n 1 | tr -d [:space:])
      appcenter apps set-current ${full_app_name}
    else
      clear
      echo "${app_name} not found in AppCenter repository."
      echo "Please make sure that the name of the app provided is written as shown below:"
      echo
      appcenter apps list | grep -v "${NON_IOS_APPS}" | awk -F/ '{print $2}'
      cleanup
      exit 1
    fi
  else
    full_app_name=$(appcenter apps list | grep -v "${NON_IOS_APPS}" | grep ${app_name} | head -n 1 | tr -d [:space:] | sed -e 's/(currentapp)//' -e 's/*//')
  fi

  # Get latest build number:
  latest_build=`appcenter distribute releases list | grep ID | awk '{ print $2 }' | sort -h | tail -n 1`

  # Download URL example:
  # https://rink.hockeyapp.net/api/2/apps/aefd482dbe5d48fdb91a09636c672819/app_versions/105?format=ipa&pltoken=4be33ffdf7efbd922643e314b62b5a5a&avtoken=c96fd65ac244f5745599c433365cc49fbd55ae4e&download_origin=hockeyapp&mctoken=744de7fd59107aed641d1c93b63635739a473dc4

  ### Bundle ID is spread out over multiple (2) lines
  # Grab Bundle ID lines
  bundle_id_array=()
  for line in `appcenter distribute releases show -r $latest_build | grep ^"Bundle Identifier" -A 1`; do
    if [[ $line != "Bundle" ]] && [[ $line != "Identifier:" ]]; then
      bundle_id_array+=($line)
    fi
  done

  # Build Bundle ID
  bundle_id=""
  for id_portion in `echo ${bundle_id_array[@]}`; do
    bundle_id+=${id_portion}
  done

  ### Download URL is spread out over multipe (7) lines.
  # Grab Download URL lines
  download_url_array=()
  for line in `appcenter distribute releases show -r $latest_build | grep ^Download -A 6`; do
    if [[ $line != "Download" ]] && [[ $line != "URL:" ]]; then
      download_url_array+=($line)
    fi
  done

  # Build Download URL
  download_url=""
  for url_portion in `echo ${download_url_array[@]}`; do
    download_url+=${url_portion}
  done

  # Download the build
  ipa_file="${temp_dir}/${app_name}.ipa"
  if ! [[ -f ${ipa_file} ]]; then
    echo "Downloading latest build for ${app_name}..."
    wget -O ${ipa_file} ${download_url} --no-verbose >/dev/null 2>&1
    if [[ -f ${ipa_file} ]]; then
      echo "Finished downloading latest ${app_name} build."
    else
      echo "Download failed!"
    fi
  else
    echo "Build for ${app_name} already downloaded."
  fi
}

get_alternative_provisioning_profile () {

  echo "Please enter full path to alternative provisioning profile to use for re-signing ${ipa_file}: "
  read alterntive_mobile_provision_file
  if [ ! -f ${alterntive_mobile_provision_file} ] || [[ ${alterntive_mobile_provision_file} == "" ]]; then
    echo "${alterntive_mobile_provision_file} not found."
    echo "Quitting..."
    cleanup
    exit 1
  else
    MOBILE_PROVISION_FILE=${alterntive_mobile_provision_file}
  fi
    echo "Attempting to sign with ${MOBILE_PROVISION_FILE}..."
}

get_signing_id () {
  # # Get Dev Signing Identity
  # signing_id=$(security find-identity -v -p codesigning | grep 'Dev User' | awk '/iPhone Developer/ { print $2 }')

  # Get Enterprise Signing ID
  signing_id=$(security find-identity -v -p codesigning | grep "iPhone Distribution: Image Metrics, Inc." | awk '{ print $2 }')

  if [[ ${signing_id} == "" ]]; then
    echo "You don't have the proper signing ID imported on this computer."
    echo "Please import a valid signing ID for iOS development first and try again."
    cleanup
    exit 1
  fi
}

clone_git_repo () {
  if [[ -d ${temp_dir}/fastlanemanagedcertificates ]]; then
    echo "Removing existing local git repo..."
    rm -rf ${temp_dir}/fastlanemanagedcertificates
  fi

  echo "Cloning git repository..."
  git clone git@bitbucket.org:ImageMetrics/fastlanemanagedcertificates.git ${temp_dir}/fastlanemanagedcertificates >/dev/null 2>&1
  if [[ $(echo $?) == 0 ]]; then
    echo "Git repository cloned successfully."
  else
    echo "Error downloading git repository."
    echo "You may not have correct access rights to the repository."
    cleanup
    exit 1
  fi
}

retrieve_certs_and_provisioning_profile () {
  case "$1" in
    # ${MOBILE_PROVISION_NAME} is taken from the names of the provision profiles in BitBucket.
    # The naming convention of the provision profiles are as follows: "InHouse_com.image-metrics-enterprise.${MOBILE_PROVISION_NAME}.mobileprovision"
    # This case statement builds the name of the provision profile based on the app name.

    ARQA-iOS)
      MOBILE_PROVISION_NAME="LiveDriverSDKQAAppAugmentedRealityiOS"
      ;;
    Asset-Previewer)
      MOBILE_PROVISION_NAME="AssetPrevieweriOS"
      ;;
    BodyDriverSDKSandboxiOS)
      MOBILE_PROVISION_NAME="BodyDriverSDKSandboxiOS"
      ;;
    Hooya-iOS)
      MOBILE_PROVISION_NAME="projectx"
      ;;
    Image-Metrics-Viewer)
      MOBILE_PROVISION_NAME="Viewer"
      ;;
    KioskAppiOS)
      MOBILE_PROVISION_NAME="KioskAppiOS"
      ;;
    LUNAR-Player)
      MOBILE_PROVISION_NAME="Lunar.Player"
      ;;
    LunarSDKSandboxiOS)
      MOBILE_PROVISION_NAME="MatryoshkaSandboxiOS"
      ;;
    Plugin-Test-App)
      MOBILE_PROVISION_NAME="plugin-test-app"
      ;;
    Speqs-iOS)
      MOBILE_PROVISION_NAME="speqs"
      ;;
    *)
      echo "No Provision Profile found for ${app_name}."
      get_alternative_provisioning_profile
      ;;
  esac

  MOBILE_PROVISION_FILE="InHouse_com.image-metrics-enterprise.${MOBILE_PROVISION_NAME}.mobileprovision"

  if [[ -d ${temp_dir}/fastlanemanagedcertificates ]]; then
    path_to_cert="${temp_dir}/fastlanemanagedcertificates/certs/enterprise/U93J3972E9.cer"
    path_to_p12="${temp_dir}/fastlanemanagedcertificates/certs/enterprise/U93J3972E9.p12"
  else
    echo "Cannot find cloned git repository."
    exit 1
  fi

  encryption_password='Creat10n!'

  # Decrypt Provisioning Profile
  openssl aes-256-cbc -k 'Creat10n!' -in ${temp_dir}/fastlanemanagedcertificates/profiles/enterprise/${MOBILE_PROVISION_FILE} \
    -out ${temp_dir}/${MOBILE_PROVISION_FILE} -a -d

  # Decrypt .cer file as a .pem file
  openssl aes-256-cbc -k ${encryption_password} -in ${path_to_cert} -out ${temp_dir}/cert.der -a -d
  openssl x509 -inform der -in ${temp_dir}/cert.der -out ${temp_dir}/cert.pem

  # Decrypt .p12 private key
  openssl aes-256-cbc -k ${encryption_password} -in ${path_to_p12} -out ${temp_dir}/key.pem -a -d

  # Generate .p12 file with cert and key
  openssl pkcs12 -export -out ${temp_dir}/cert.p12 -inkey ${temp_dir}/key.pem -in ${temp_dir}/cert.pem -password pass:${encryption_password}

  # Install combined .p12 file into login keychain
  security import ${temp_dir}/cert.p12 -k ~/Library/Keychains/login.keychain-db -P Creat10n! -T /usr/bin/codesign >/dev/null 2>&1
  if [[ $(echo $?) == 0 ]]; then
    echo "Certificate imported successfully"
  else
    echo "There was an error importing the certificate."
    cleanup
    exit 1
  fi

  ### Retrieve a Signing ID ###
  get_signing_id
}

resign_app () {
  # Resign app
  echo "Resigning app..."
  fastlane sigh resign ${ipa_file} --signing_identity ${signing_id} --provisioning_profile \
    ${temp_dir}/${MOBILE_PROVISION_FILE} >/dev/null 2>&1
  if [[ $(echo $?) == 0 ]]; then
    echo "[$(date +%T)]: Successfully signed ${ipa_file}!"
  else
    # Attempt a different, manually downloaded provision profile
    echo "[$(date +%T)]: Something went wrong while code signing ${ipa_file}..."

    get_alternative_provisioning_profile
    fastlane sigh resign ${ipa_file} --signing_identity ${signing_id} --provisioning_profile \
      ${MOBILE_PROVISION_FILE} >/dev/null 2>&1

    if [[ $(echo $?) == 0 ]]; then
      echo "[$(date +%T)]: Successfully signed ${ipa_file}!"
    else
      # If code sign fails again with alternate provision profile, exit script.
      echo "[$(date +%T)]: Something went wrong while code signing ${ipa_file} with alternative mobile provision profile..."
      echo "Quitting..."
      cleanup
      exit 1
    fi
  fi
}

upload_build () {
  # Upload release binary and trigger distribution

  # owner_name=$(appcenter apps show | awk '/Owner Name/ { print $3 }')

  # Get first distribution group for app
  # 'appcenter distribute groups list' outputs groups in order of number of members.
  # Selected distribution group has most number of members.
  distribution_group=$(appcenter distribute groups list | sed -n '4p' | awk '{ print $2 }')

  clear
  echo "Uploading resigned app back up to AppCenter..."
  appcenter distribute release --file ${ipa_file} --silent --build-number ${latest_build} \
    --app ${full_app_name} --group ${distribution_group}

  if [[ $(echo $?) == 3 ]]; then
    echo "Error uploading release for ${ipa_file}."
    echo "Possible \"statusCode 403: Forbidden\" error."
    cleanup
    exit 1
  fi
}

temp_dir_creation () {

  temp_dir="/tmp/im-ios-resign-${app_name}"

  if ! [[ -d ${temp_dir} ]]; then
    mkdir -p ${temp_dir}
  fi
}

### Check that dependencies are installed ###
get_dependencies

### Verify if user is logged in ###
verify_login_status

### Check that user has inputted application name ###
check_number_of_arguments ${app_name}

### Create temporary directory ###
temp_dir_creation

### Download git repository ###
clone_git_repo

### Download app build ###
download_build

### Retrieve certs and provisioning profile ###
retrieve_certs_and_provisioning_profile ${app_name}

### Resign ipa file ###
resign_app

### Upload Build ###
upload_build

### Remove temporary files ###
cleanup

echo "Done!"

# appcenter-cli commands:
# https://github.com/microsoft/appcenter-cli/blob/master/README.md